package fr.example.myapplication.data;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import fr.example.myapplication.ListFragmentDirections;
import fr.example.myapplication.R;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    Country[] bisCountries=Country.countries;

    public static class ViewHolder extends RecyclerView.ViewHolder{

        ImageView itemImage;
        TextView itemCountry;
        TextView itemCapital;
        View itemView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemImage   = itemView.findViewById(R.id.item_image);
            itemCountry = itemView.findViewById(R.id.item_country);
            itemCapital = itemView.findViewById(R.id.item_capital);
            this.itemView=itemView;
            int position=getAdapterPosition();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=getAdapterPosition();
                    ListFragmentDirections.ActionFirstFragmentToSecondFragment action=ListFragmentDirections.actionFirstFragmentToSecondFragment();
                    action.setCountryId(position);
                    Navigation.findNavController(v).navigate(action);
                }
            });
        }

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v=LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout,parent,false);
        ViewHolder viewHolder=new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Country country=bisCountries[position];
        String uri=country.getImgUri();
        Context c=holder.itemView.getContext();
        holder.itemImage.setImageResource(c.getResources().getIdentifier(uri,null,c.getPackageName()));
        holder.itemCountry.setText(country.getName());
        holder.itemCapital.setText(country.getCapital());
    }

    @Override
    public int getItemCount() {
        return bisCountries.length;
    }


}
