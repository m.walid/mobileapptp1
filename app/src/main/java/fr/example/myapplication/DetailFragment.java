package fr.example.myapplication;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import fr.example.myapplication.data.Country;

public class DetailFragment extends Fragment {
    Country[] country=Country.countries;
    ImageView imageView;
    TextView tCountry;
    TextView tCapital;
    TextView tLanguage;
    TextView tCurrency;
    TextView tPopulation;
    TextView tSuperficie;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        imageView=view.findViewById(R.id.item_image1);
        tCountry=view.findViewById(R.id.item_country1);
        tCapital=view.findViewById(R.id.item_capital2);
        tLanguage=view.findViewById(R.id.item_langue);
        tCurrency=view.findViewById(R.id.item_monnaie);
        tPopulation=view.findViewById(R.id.item_population);
        tSuperficie=view.findViewById(R.id.item_superficie);

        if(getArguments() != null) {
            DetailFragmentArgs dfa = DetailFragmentArgs.fromBundle(getArguments());
            int position=dfa.getCountryId();
            Country country1=country[position];
            String uri=country1.getImgUri();
            Context c=view.getContext();
            imageView.setImageResource(c.getResources().getIdentifier(uri,null,c.getPackageName()));
            tCountry.setText(country1.getName());
            tCapital.setText(country1.getCapital());
            tLanguage.setText(country1.getLanguage());
            tCurrency.setText(country1.getCurrency());
            tPopulation.setText(String.valueOf(country1.getPopulation()));
            tSuperficie.setText(String.valueOf(country1.getArea())+" km2");

        }





        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}